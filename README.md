Dalam tutorial ini, saya akan membangun Restful API CRUD (Create, Retrieve, Update, Delete) dengan Node.js, Express dan MongoDB. Saya akan menggunakan Mongoose untuk berinteraksi dengan MongoDB.

Express adalah salah satu kerangka kerja web paling populer saat ini untuk node.js. Dibangun di atas modul http node.js, dan menambahkan dukungan untuk routes, middleware, view system dll. Express sangat sederhana dan minimal, tidak seperti kerangka kerja lain yang mencoba melakukan banyak hal, sehingga dapat mengurangi fleksibilitas bagi pengembang untuk memiliki pilihan desain sendiri.

Mongoose adalah alat ODM (Object Document Mapping) untuk Node.js dan MongoDB. Ini membantu Anda mengonversi objek dalam kode Anda ke dokumen dalam database dan juga sebaliknya.

Sebelum melanjutkan ke bagian berikutnya, Silakan instal MongoDB di pc/laptop Anda jika Anda belum melakukannya. Periksa <a href="https://docs.mongodb.com/manual/administration/install-community/">manual/documentasi</a> instalasi MongoDB resmi untuk bantuan apa pun dalam proses instalasinya.
