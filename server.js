const express = require('express');
const bodyParser = require('body-parser');

//Membuat express app
const app = express()

//parse request of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended:true}))

//parse request content-type -appliction/json
app.use(bodyParser.json())
//configuring databasenya
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

//connecting ke database
mongoose.connect(dbConfig.url, {
    userNewUrlParser: true
}).then(() => {
    console.log("Success connected ke database");
}).catch(err => {
    console.log('Tidak bisa connect  ke database. Exitting now...', err);
    process.exit();
});

//define a simple route
app.get('/', (req, res) => {
    res.json({"message" : "Selamat datang di EasyNotes apps. Tulis notes"})
});

//Require Notes Routes
require('./app/routes/note.routes.js')(app);

//listen untuk requests
app.listen(3001, () => {
    console.log("Kamu sedang menjalankan port 3001");
});