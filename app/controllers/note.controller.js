const Note = require('../models/note.model.js');

// Membuat fungsi buat dan save note baru


exports.create = (req, res) => {
// Validate request

if(!req.body.content) {
    return res.status(400).send({
        message: "Note content tidak boleh kosong"
    });
}

//membuat sebuah note
const note = new Note ({
    title: req.body.title || "Untitled Note",
    content: req.body.content
});

note.save()
.then(data => {
    res.send(data);
}).catch(err => {
    res.status(500).send({
        message: err.message || "Terjadi error ketika membuat note"
    });
});

};


// Retrieve dan mengembalikan semua note dari database

exports.findAll = (req, res) => {
    Note.find()
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Terjadi error ketika mencoba retrieve note"
        });
    });

};

// Mencari sebuah note dengan noteId
exports.findOne = (req, res) => {
    Note.findById(req.params.noteId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note dengan id " + req.params.noteId + " tidak ditemukan"
            });
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note dengan id " + req.params.noteId + " tidak ditemukan"
            });
        }
        return res.status(500).send({
            message: "Error retriving note dengan id " + req.params.noteId
        });
    });

};

// Update sebuah note dari noteID
exports.update = (req,res) => {
    //Validate request
    if(!req.body.content) {
        return res.status(400).send({
            message: "Isi note tidak boleh kosong"
        });
    }
// Cari note dan update dengan request body
Note.findByIdAndUpdate(req.params.noteId, {
    title: req.body.title || "Untitled Note",
    content: req.body.content
}, {new: true})
.then(note => {
    if(!note) {
        return res.status(404).send({
            message: "Note dengan id " + req.params.noteId + " tidak ditemukan"
        });
    }
    res.send(note);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404). send ({
            message: "Note tidak ditemukan dengan id " +req.params.noteId
        });
    }
    return res.status(500).send({
        message: "Error ketika updating note dengan id " + req.params.noteId
    });
});
};


// Delete sebuah note dengan noteID
exports.delete = (req, res) => {
 Note.findByIdAndRemove(req.params.noteId)
 .then(note => {
     if(!note) {
         return res.status(404).send({
             message: "Note dengan id " + req.params.noteId + " tidak ditemukan"
         });
     }
     res.send({message: "Note telah success terhapus!"});
 }).catch(err => {
     if(err.kind === 'ObjectId' || err.name === 'NotFound') {
         return res.status(404).send({
             message: "Note dengan id " + req.params.noteId + " tidak ditemukan"
         });
     }
     return res.status(500).send({
         messsage: "Tidak bisa mendelete note dengan id " + req.params.noteId
     });
 });
};