module.exports = (app) => {
    const notes = require ('../controllers/note.controller.js');

//Membuat /create new Note
app.post('/notes', notes.create);


//Mengambil semua data /retieve all Notes
app.get('/notes', notes.findAll);


//Mengambil satu Note denga noteId
app.get('/notes/:noteId', notes.findOne);

//Mengupdate note dengan NoteID
app.put('/notes/:noteId', notes.update);

//Delete sebuah note dengan noteId
app.delete('/notes/:noteId', notes.delete);

}